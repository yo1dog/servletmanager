package net.awesomebox.servletmanager;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import net.awesomebox.servletmanager.exceptions.InvalidHTTPRequestParameterException;
import net.awesomebox.servletmanager.exceptions.MissingHTTPRequestParameterException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Provides helper methods for common servlet tasks.
 * 
 * @author michael.moore
 */
public class ServletHelper
{
	//================================================================================
	// Printing
	//================================================================================
	
	//--------------------------------------------------------------------------------
	// Plain
	//--------------------------------------------------------------------------------
	
	/**
	 * Writes a string to the response as plain text.
	 * 
	 * @param response
	 * 	HTTP response to write to.
	 * @param s
	 * 	String to write.
	 * 
	 * @throws IOException
	 */
	public static void printPlainText(HttpServletResponse response, Object o) throws IOException
	{
		response.setContentType("text/plain");
		response.getWriter().print(o);
	}
	
	
	
	//--------------------------------------------------------------------------------
	// JSON
	//--------------------------------------------------------------------------------
	
	/**
	 * Default GSON builder to create JSON.
	 */
	private static final Gson gson;
	static
	{
		gson = new GsonBuilder().serializeNulls().create();
	}
	
	/**
	 * Writes an object as JSON to the response.
	 * 
	 * @param response
	 * 	HTTP response to write to.
	 * @param object
	 * 	Object to convert to JSON and write.
	 * 
	 * @throws IOException
	 */
	public static void printJSON(HttpServletResponse response, Object object) throws IOException
	{
		printJSON(response, object, gson);
	}
	/**
	 * Writes an object as JSON to the response.
	 * 
	 * @param response
	 * 	HTTP response to write to.
	 * @param object
	 * 	Object to convert to JSON and write.
	 * @param gson
	 * 	GSON builder to use to create JSON.
	 * 
	 * @throws IOException
	 */
	public static void printJSON(HttpServletResponse response, Object object, Gson gson) throws IOException
	{
		response.setContentType("application/json");    
		PrintWriter out = response.getWriter();
		
		out.print(gson.toJson(object));
	}
	
	
	
	//================================================================================
	// Parameter Retrieving
	//================================================================================
	
	//--------------------------------------------------------------------------------
	// Optional
	//--------------------------------------------------------------------------------
	
	/**
	 * Returns an HTTP request parameter's value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Value of the parameter or <code>null</code> if it is missing from the request.
	 * 
	 * @see javax.servlet.ServletRequest#getParameter(String)
	 */
	public static String getParameter(HttpServletRequest request, String parameterName)
	{
		return getParameter(request, parameterName, null);
	}
	/**
	 * Returns an HTTP request parameter's value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * @param defaultValue
	 * 	Value to return if the parameter is missing form the request.
	 * 
	 * @return
	 * 	Value of the parameter or <code>defaultValue</code> if it is missing from the request.
	 * 
	 * @see javax.servlet.ServletRequest#getParameter(String)
	 */
	public static String getParameter(HttpServletRequest request, String parameterName, String defaultValue)
	{
		String value = request.getParameter(parameterName);
		
		return value == null ? defaultValue : value;
	}
	
	/**
	 * Returns an HTTP request parameter's value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Value of the parameter or <code>null</code> if it is missing from the request
	 * 	or is empty.
	 * 
	 * @see javax.servlet.ServletRequest#getParameter(String)
	 */
	public static String getParameterNotEmpty(HttpServletRequest request, String parameterName)
	{
		return getParameterNotEmpty(request, parameterName, null);
	}
	/**
	 * Returns an HTTP request parameter's value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * @param defaultValue
	 * 	Value to return if the parameter is missing form the request.
	 * 
	 * @return
	 * 	Value of the parameter or <code>defaultValue</code> if it is missing from the request
	 * 	or is empty.
	 * 
	 * @see javax.servlet.ServletRequest#getParameter(String)
	 */
	public static String getParameterNotEmpty(HttpServletRequest request, String parameterName, String defaultValue)
	{
		String value = getParameter(request, parameterName);
		
		if (value == null || value.length() == 0)
			return defaultValue;
		
		return value;
	}
	
	/**
	 * Returns an HTTP request parameter's integer value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Integer value of the parameter or <code>null</code> if it is missing from the request.
	 * 
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid integer.
	 */
	public static Integer getParameterInt(HttpServletRequest request, String parameterName) throws InvalidHTTPRequestParameterException
	{
		return getParameterInt(request, parameterName, null);
	}
	/**
	 * Returns an HTTP request parameter's integer value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * @param defaultValue
	 * 	Value to return if the parameter is missing from the request.
	 * 
	 * @return
	 * 	Integer value of the parameter or <code>defaultValue</code> if it is missing from the request.
	 * 
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid integer.
	 */
	public static Integer getParameterInt(HttpServletRequest request, String parameterName, Integer defaultValue) throws InvalidHTTPRequestParameterException
	{
		String value = getParameterNotEmpty(request, parameterName);
		
		if (value == null)
			return defaultValue;
		
		try
		{
			return Integer.parseInt(value);
		}
		catch (NumberFormatException e)
		{
			throw new InvalidHTTPRequestParameterException(parameterName, "Not a valid integer.");
		}
	}
	
	/**
	 * Returns an HTTP request parameter's short value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Short value of the parameter or <code>null</code> if it is missing from the request.
	 * 
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid short.
	 */
	public static Short getParameterShort(HttpServletRequest request, String parameterName) throws InvalidHTTPRequestParameterException
	{
		return getParameterShort(request, parameterName, null);
	}
	/**
	 * Returns an HTTP request parameter's short value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * @param defaultValue
	 * 	Value to return if the parameter is missing from the request.
	 * 
	 * @return
	 * 	Short value of the parameter or <code>defaultValue</code> if it is missing from the request.
	 * 
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid short.
	 */
	public static Short getParameterShort(HttpServletRequest request, String parameterName, Short defaultValue) throws InvalidHTTPRequestParameterException
	{
		String value = getParameterNotEmpty(request, parameterName);
		
		if (value == null)
			return defaultValue;
		
		try
		{
			return Short.parseShort(value);
		}
		catch (NumberFormatException e)
		{
			throw new InvalidHTTPRequestParameterException(parameterName, "Not a valid short.");
		}
	}
	
	/**
	 * Returns an HTTP request parameter's boolean value. Value must be "true" or "false" case insensitive.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Boolean value of the parameter or <code>null</code> if it is missing from the request.
	 * 
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid boolean.
	 */
	public static Boolean getParameterBoolean(HttpServletRequest request, String parameterName) throws InvalidHTTPRequestParameterException
	{
		return getParameterBoolean(request, parameterName, null);
	}
	/**
	 * Returns an HTTP request parameter's boolean value. Value must be "true" or "false" case insensitive.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * @param defaultValue
	 * 	Value to return if the parameter is missing from the request.
	 * 
	 * @return
	 * 	Boolean value of the parameter or <code>defaultValue</code> if it is missing from the request.
	 * 
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid boolean.
	 */
	public static Boolean getParameterBoolean(HttpServletRequest request, String parameterName, Short defaultValue) throws InvalidHTTPRequestParameterException
	{
		String value = getParameterNotEmpty(request, parameterName);
		
		if (value == null)
			return null;
		
		value = value.toLowerCase();
		
		if (value.equals("true"))
			return true;
		else if (value.equals("false"))
			return false;
		else
			throw new InvalidHTTPRequestParameterException(parameterName, "Not a valid boolean.");
	}
	
	
	
	//--------------------------------------------------------------------------------
	// Required
	//--------------------------------------------------------------------------------
	
	/**
	 * Returns an HTTP request parameter's value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Value of the parameter.
	 * 
	 * @throws MissingHTTPRequestParameterException
	 * 	The parameter is missing from the request.
	 */
	public static String getRequiredParameter(HttpServletRequest request, String parameterName) throws MissingHTTPRequestParameterException
	{
		String value = getParameter(request, parameterName);
		
		if (value == null)
			throw new MissingHTTPRequestParameterException(parameterName);
		
		return value;
	}
	
	/**
	 * Returns an HTTP request parameter's value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Value of the parameter.
	 * 
	 * @throws MissingHTTPRequestParameterException
	 * 	The parameter is missing or empty in the request.
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter is empty.
	 */
	public static String getRequiredParameterNotEmpty(HttpServletRequest request, String parameterName) throws MissingHTTPRequestParameterException, InvalidHTTPRequestParameterException
	{
		String value = getRequiredParameter(request, parameterName);
		
		if (value.length() == 0)
			throw new InvalidHTTPRequestParameterException(parameterName, "Can not be empty.");
		
		return value;
	}
	
	/**
	 * Returns an HTTP request parameter's integer value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Integer value of the parameter.
	 * 
	 * @throws MissingHTTPRequestParameterException
	 * 	The parameter is missing or empty in the request.
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid integer. 
	 */
	public static int getRequiredParameterInt(HttpServletRequest request, String parameterName) throws MissingHTTPRequestParameterException, InvalidHTTPRequestParameterException
	{
		String value = getRequiredParameterNotEmpty(request, parameterName);
		
		try
		{
			return Integer.parseInt(value);
		}
		catch (NumberFormatException e)
		{
			throw new InvalidHTTPRequestParameterException(parameterName, "Not a valid integer.");
		}
	}
	
	/**
	 * Returns an HTTP request parameter's short value.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Short value of the parameter.
	 * 
	 * @throws MissingHTTPRequestParameterException
	 * 	The parameter is missing or empty in the request.
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid short. 
	 */
	public static short getRequiredParameterShort(HttpServletRequest request, String parameterName) throws MissingHTTPRequestParameterException, InvalidHTTPRequestParameterException
	{
		String value = getRequiredParameterNotEmpty(request, parameterName);
		
		try
		{
			return Short.parseShort(value);
		}
		catch (NumberFormatException e)
		{
			throw new InvalidHTTPRequestParameterException(parameterName, "Not a valid short.");
		}
	}
	
	/**
	 * Returns an HTTP request parameter's boolean value. Value must be "true" or "false" insensitive.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param parameterName
	 * 	Name of the parameter to get the value of.
	 * 
	 * @return
	 * 	Boolean value of the parameter.
	 * 
	 * @throws MissingHTTPRequestParameterException
	 * 	The parameter is missing or empty in the request.
	 * @throws InvalidHTTPRequestParameterException
	 * 	The parameter's value is not a valid boolean.
	 */
	public static boolean getRequiredParameterBoolean(HttpServletRequest request, String parameterName) throws MissingHTTPRequestParameterException, InvalidHTTPRequestParameterException
	{
		String value = getRequiredParameterNotEmpty(request, parameterName).toLowerCase();
		
		if (value.equals("true"))
			return true;
		else if (value.equals("false"))
			return false;
		else
			throw new InvalidHTTPRequestParameterException(parameterName, "Not a valid boolean.");
	}
	
	/**
	 * Returns a multipart content encrypted HTTP request part.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param partNmae
	 * 	Name of the part to get the value of.
	 * 
	 * @return
	 * 	Multipart content encrypted HTTP request part.
	 * 
	 * @throws MissingHTTPRequestParameterException
	 * 	The part is missing from the request.
	 * @throws ServletException
	 * @throws IOException 
	 * @throws IllegalStateException
	 */
	public static Part getRequiredPart(HttpServletRequest request, String partName) throws IllegalStateException, IOException, ServletException
	{
		Part part = request.getPart(partName);
		
		if (part == null)
			throw new MissingHTTPRequestParameterException();
		
		return part;
	}
	
	/**
	 * Returns a multipart content encrypted HTTP request part.
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param partNmae
	 * 	Name of the part to get the value of.
	 * 
	 * @return
	 * 	Multipart content encrypted HTTP request part.
	 * 
	 * @throws MissingHTTPRequestParameterException
	 * 	The part is missing from the request.
	 * @throws ServletException
	 * @throws IOException 
	 * @throws IllegalStateException
	 */
	public static Part getRequiredPartNotEmpty(HttpServletRequest request, String partName) throws IllegalStateException, IOException, ServletException
	{
		Part part = getPartNotEmpty(request, partName);
		
		if (part == null)
			throw new MissingHTTPRequestParameterException();
		
		return part;
	}
	
	/**
	 * Returns a multipart content encrypted HTTP request part or <code>null</code> if
	 * the part is missing from the request or the the part is empty (0 bytes).
	 * 
	 * @param request
	 * 	HTTP request.
	 * @param partNmae
	 * 	Name of the part to get the value of.
	 * 
	 * @return
	 * 	Multipart content encrypted HTTP request part.
	 * 
	 * @throws ServletException
	 * @throws IOException 
	 * @throws IllegalStateException
	 */
	public static Part getPartNotEmpty(HttpServletRequest request, String partName) throws IllegalStateException, IOException, ServletException
	{
		Part part = request.getPart(partName);
		
		if (part == null || part.getSize() == 0)
			return null;
		
		return part;
	}
	
	
	public static String escapeHTML(String html)
	{
		if (html == null)
			return null;
		
		return html
			.replace("&", "&amp;")
			.replace("\"", "&quot;")
			.replace("<", "&lt;")
			.replace(">", "&gt;");
	}
	
	public static String newlinesToBR(String html)
	{
		if (html == null)
			return null;
		
		return html
			.replace("\r\n", "<br />")
			.replace("\n", "<br />");
	}
}
