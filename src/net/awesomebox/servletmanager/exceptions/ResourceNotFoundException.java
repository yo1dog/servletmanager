package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;


/**
 * A resource was not found.
 * 
 * @author michael.moore
 */
public class ResourceNotFoundException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;

	public ResourceNotFoundException()
	{
		super(404, "Resource not found.");
	}
	
	public ResourceNotFoundException(String message)
	{
		super(404, "Resource not found. " + message);
	}
	
	public ResourceNotFoundException(String message, Throwable rootCause)
	{
		super(404, "Resource not found. " + message, rootCause);
	}
}
