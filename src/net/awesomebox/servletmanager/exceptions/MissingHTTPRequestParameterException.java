package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;


/**
 * A required parameter is missing from an HTTP request.
 * 
 * @author michael.moore
 */
public class MissingHTTPRequestParameterException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;

	public MissingHTTPRequestParameterException()
	{
		super(400, "Missing parameter.");
	}
	
	public MissingHTTPRequestParameterException(String parameterName)
	{
		super(400, "Missing parameter \"" + parameterName + "\".");
	}
	
	public MissingHTTPRequestParameterException(String parameterName, String message, Throwable rootCause)
	{
		super(400, "Missing parameter \"" + parameterName + "\". " + message, rootCause);
	}
}
