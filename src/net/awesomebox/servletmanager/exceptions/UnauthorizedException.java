package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;


/**
 * A resource was not found.
 * 
 * @author michael.moore
 */
public class UnauthorizedException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;

	public UnauthorizedException()
	{
		super(401, "Unauthorized.");
	}
	
	public UnauthorizedException(String message)
	{
		super(401, "Unauthorized. " + message);
	}
	
	public UnauthorizedException(String message, Throwable rootCause)
	{
		super(401, "Unauthorized. " + message, rootCause);
	}
}
