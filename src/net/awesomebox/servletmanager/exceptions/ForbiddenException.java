package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;


/**
 * A resource was not found.
 * 
 * @author michael.moore
 */
public class ForbiddenException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;

	public ForbiddenException()
	{
		super(403, "Forbidden.");
	}
	
	public ForbiddenException(String message)
	{
		super(403, "Forbidden. " + message);
	}
	
	public ForbiddenException(String message, Throwable rootCause)
	{
		super(403, "Forbidden. " + message, rootCause);
	}
}
