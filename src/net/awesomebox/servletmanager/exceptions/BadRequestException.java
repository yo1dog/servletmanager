package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;


/**
 * An HTTP request is invalid. 
 * 
 * @author michael.moore
 */
public class BadRequestException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;

	public BadRequestException()
	{
		super(400, "Bad request.");
	}
	
	public BadRequestException(String message)
	{
		super(400, "Bad request. " + message);
	}
}
