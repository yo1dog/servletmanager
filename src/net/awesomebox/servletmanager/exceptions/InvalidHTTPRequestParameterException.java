package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;


/**
 * A parameter is invalid in an HTTP request.
 * 
 * @author michael.moore
 */
public class InvalidHTTPRequestParameterException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;

	public InvalidHTTPRequestParameterException()
	{
		super(400, "Invalid parameter.");
	}
	
	public InvalidHTTPRequestParameterException(String parameterName)
	{
		super(400, "Invalid parameter \"" + parameterName + "\".");
	}
	
	public InvalidHTTPRequestParameterException(String parameterName, String message)
	{
		super(400, "Invalid parameter \"" + parameterName + "\". " + message);
	}
	
	public InvalidHTTPRequestParameterException(String parameterName, String message, Throwable rootCause)
	{
		super(400, "Invalid parameter \"" + parameterName + "\". " + message, rootCause);
	}
}
