package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;



/**
 * Represents an error with a value used in a SQL statement.
 * 
 * @author michael.moore
 */
public class ResourceAlreadyExistsException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;
	
	public ResourceAlreadyExistsException()
	{
		super(409, "Resource already exists.");
	}
	
	public ResourceAlreadyExistsException(String message)
	{
		super(409, "Resource already exists. " + message);
	}
	
	public ResourceAlreadyExistsException(String message, Throwable rootCause)
	{
		super(409, "Resource already exists. " + message, rootCause);
	}
}
