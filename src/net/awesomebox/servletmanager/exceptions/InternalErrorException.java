package net.awesomebox.servletmanager.exceptions;

import net.awesomebox.servletmanager.ManagedServletException;


/**
 * A parameter is invalid in an HTTP request.
 * 
 * @author michael.moore
 */
public class InternalErrorException extends ManagedServletException
{
	private static final long serialVersionUID = 1L;

	@Override
	public boolean getShouldLogStackTrace()
	{
		return true;
	}
	
	
	public InternalErrorException()
	{
		this(null, null);
	}
	
	public InternalErrorException(String message)
	{
		this(message, null);
	}
	
	public InternalErrorException(Throwable rootCause)
	{
		this(null, rootCause);
	}
	
	public InternalErrorException(String message, Throwable rootCause)
	{
		super(500, message != null ? message : "An internal error occurred.", rootCause);
	}
}
