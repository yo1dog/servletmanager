package net.awesomebox.servletmanager;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Enhances the {@link javax.servlet.http.HttpServlet HttpServlet} class to add special exception handling.
 * {@link net.awesomebox.servletmanager.ManagedServletException ManagedServletException}
 * are caught and an error page is displayed based on its {@link net.awesomebox.servletmanager.ManagedServletException#statusCode statusCode}
 * and message. {@link java.io.IOException IOException} and {@link javax.servlet.ServletException ServletException} flow through as normal.
 * Use {@link #beforeRequests} and {@link #afterRequests} hooks to implement 
 * <br />
 * <strong>Note:</strong> Override {@link #_doGet}, {@link #_doPost}, {@link #_doPut}, and {@link #_doDelete}.
 * 
 * @author michael.moore
 * 
 * @see net.awesomebox.servletmanager.ManagedServletException ExceptionHelperServletException
 */
public class ManagedHttpServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	//================================================================================
    // Request Handling
	//================================================================================
	
	public static enum HttpMethod
	{
		GET,
		POST,
		PUT,
		DELETE
	}
	
	@Override
	protected final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doRequest(request, response, HttpMethod.GET);
	}
	
	@Override
	protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doRequest(request, response, HttpMethod.POST);
	}
	
	@Override
	protected final void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doRequest(request, response, HttpMethod.PUT);
	}
	
	@Override
	protected final void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doRequest(request, response, HttpMethod.DELETE);
	}
	
	private final void doRequest(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws ServletException, IOException
	{
		try
		{
			beforeRequests(request, response, method);
			
			try
			{
				switch (method)
				{
					case GET:
						_doGet(request, response);
						break;
					
					case POST:
						_doPost(request, response);
						break;
					
					case PUT:
						_doPut(request, response);
						break;
					
					case DELETE:
						_doDelete(request, response);
						break;
					
					default:
						response.sendError(405);
				}
			}
			finally
			{
				afterRequests(request, response, method);
			}
		}
		catch (ManagedServletException e)
		{
			// handle managed exception
			boolean handled = false;
			try
			{
				handled = onManagedServletException(request, response, method, e);
			}
			catch (ServletException|IOException e2)
			{
				// error while handling
				e.printStackTrace();
				throw e2;
			}
			
			if (!handled)
				throw e;
		}
		catch (Exception e)
		{
			// handle other exceptions
			boolean handled = false;
			try
			{
				handled = onException(request, response, method, e);
			}
			catch (ServletException|IOException e2)
			{
				// error while handling
				e.printStackTrace();
				throw e2;
			}
			
			if (!handled)
				throw new ServletException(e);
		}
	}
	
	
	protected void _doGet(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		super.doGet(request, response);
	}
	
	protected void _doPost(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		super.doPost(request, response);
	}
	
	protected void _doPut(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		super.doPut(request, response);
	}
	
	protected void _doDelete(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		super.doDelete(request, response);
	}
	
	
	protected void beforeRequests(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws Exception
	{
	}
	
	protected void afterRequests(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws Exception
	{
	}
	
	
	protected boolean onManagedServletException(HttpServletRequest request, HttpServletResponse response, HttpMethod method, ManagedServletException e) throws ServletException, IOException
	{
		if (e.getShouldLogStackTrace() || ConfigManager.config.DEBUG)
			e.printStackTrace();
		
		ErrorPage.showErrorPage(response, e);
		
		return true;
	}
	
	protected boolean onException(HttpServletRequest request, HttpServletResponse response, HttpMethod method, Exception e) throws ServletException, IOException
	{
		return false;
	}
}
