package net.awesomebox.servletmanager;

public final class ConfigManager
{
	static Config config = new Config(new ConfigTemplate());
	
	public static void setConfig(ConfigTemplate configTemplate)
	{
		ConfigManager.config = new Config(configTemplate);
	}
}
