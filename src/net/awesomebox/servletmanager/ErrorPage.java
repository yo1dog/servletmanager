package net.awesomebox.servletmanager;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;


public class ErrorPage
{
	public static void showErrorPage(HttpServletResponse response, ManagedServletException exception) throws IOException
	{
		response.setStatus(exception.getHTTPStatusCode());
		response.setContentType("text/plain");
		
		PrintWriter writer = response.getWriter();
		
		writer.println(exception.getMessage());
		
		if (ConfigManager.config.DEBUG)
		{
			if (exception.getCause() != null)
			{
				writer.println();
				writer.println();
				writer.println(" ----- Caused By ----- ");
				
				// print root cause messages
				int level = 1;
				for (Throwable cause = exception.getCause(); cause != null; cause = cause.getCause())
				{
					if (level > 1)
						writer.println();
					
					writer.println(level + ":");
					writer.println(cause.getMessage());
					
					++level;
				}
			}
			
			writer.println();
			writer.println();
			writer.println(" ----- Stacktrace ----- ");
			
			exception.printStackTrace(writer);
		}
		
		response.flushBuffer();
	}
}
