package net.awesomebox.servletmanager;

import javax.servlet.ServletException;

/**
 * Special exception that results in an error page based on the {@link #statusCode}.
 * 
 * @author michael.moore
 */
public class ManagedServletException extends ServletException
{
	private static final long serialVersionUID = 1L;
	
	private final int statusCode;
	
	
	public int getHTTPStatusCode()
	{
		return statusCode;
	}
	
	public boolean getShouldLogStackTrace()
	{
		return false;
	}
	
	
	public ManagedServletException()
	{
		super();
		this.statusCode = 500;
	}
	
	public ManagedServletException(int statusCode)
	{
		super();
		this.statusCode = statusCode;
	}
	
	public ManagedServletException(String message)
	{
		super(message);
		this.statusCode = 500;
	}
	
	public ManagedServletException(int statusCode, String message)
	{
		super(message);
		this.statusCode = statusCode;
	}
	
	public ManagedServletException(String message, Throwable rootCause)
	{
		super(message, rootCause);
		this.statusCode = 500;
	}
	
	public ManagedServletException(int statusCode, String message, Throwable rootCause)
	{
		super(message, rootCause);
		this.statusCode = statusCode;
	}
	
	public ManagedServletException(Throwable rootCause)
	{
		super(rootCause);
		this.statusCode = 500;
	}
	
	public ManagedServletException(int statusCode, Throwable rootCause)
	{
		super(rootCause);
		this.statusCode = statusCode;
	}
}
