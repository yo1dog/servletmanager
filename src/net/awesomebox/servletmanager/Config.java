package net.awesomebox.servletmanager;

final class Config
{
	public final boolean DEBUG;
	
	Config(ConfigTemplate configTemplate)
	{
		this.DEBUG = configTemplate.debug;
	}
}